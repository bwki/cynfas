# README #

__________          __   .__ 
\______   \__  _  _|  | _|__|
 |    |  _/\ \/ \/ /  |/ /  |
 |    |   \ \     /|    <|  |
 |______  /  \/\_/ |__|_ \__|
        \/              \/   
		

### What is it? ###

This is part of an old exhibition that I’ve decided to open-source & provide free, ex-gratis and for nothing.

It’s an interactive book system that uses a webcam to read markers from the pages of a book, triggering media files and micro controller signals (Arduino compatible boards only). So for instance, you could print a book containing fiducial markers with a web cam pointed at the pages, as you turn each page the camera reads the markers and triggers video files and physical objects that compliment the contents of the page.

The software is written in Max/MSP/Jitter and a tiny sprinkle of C, the micro controller interface is handled by Maxuino and is intuitive enough to get working with a basic knowledge of Arduino development. If you’d like to trigger mains powered electronics from the software/ controller you’ll need a mains relay, I strongly recommend you get the help of an experienced electronics engineer when playing with mains electricity.


### Requirements ###

An Intel® Mac with Mac OS X 10.7 (or later), OR a PC with Windows 7 (or later);

Max/MSP 5+ (version 6/7 recommended)

Multicore processor; 2 GB RAM;

1024×768 display;

Quicktime for Windows.

A webcam (The PlayStation Eye is easy to cut apart and hide in other objects, cheap and available in most second-hand stores).

The Maxuino external plugin (only supports 32 bit systems)

The TUIO external plugin for Max/MSP, contains the library and fiducial markers

You’ll also need a set of fiducial markers from reacTIVision to trigger TUIO messages.


### How do I get set up? ###

Once you’ve installed Max/MSP import the TUIO library to your ‘packages’ folder (you can pick a folder manually in Max 5/6), then connect a webcam & print as many fiducial markers as needed. Launch Cynfas and you’ll get a black screen (the video output window), hit escape and you’ll be greeted by the patch.

Inside the Cynfas media folder, replace the .MOV files 1-11 with your own media files, connect a webcam to your machine, run the app and place the fiducial markers in front of the camera. As if by magic, the media files should play corresponding to the serial number of the marker that’s displayed (e.g. marker 0 will play 0.mov, marker 1 will play 1.mov, etc…).

So there you go, if you’re looking to make a gallery exhibit, cheat on your media design course, or just make a cool toy for your home, this is a good entry in to interactive art. If you make something really amazing with this code then get in touch & I’ll send over a goodie bag of art and toys.

More details along with illustrations here - http://bwki.co.uk/portfolio/cynfas/


### Who do I talk to? ###

Contact me (Ben) at hello@bwki.co.uk